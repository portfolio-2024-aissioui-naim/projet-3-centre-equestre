# Projet Centre Equestre

Ce projet est une application de gestion pour un haras. Il implémente une classe `Cheval` pour représenter les chevaux et fournit une démonstration de son utilisation via la classe `Program`.

## Structure du Projet

Le projet est organisé en deux fichiers principaux :

- `Cheval.cs`: Ce fichier contient la définition de la classe `Cheval`, qui représente les caractéristiques d'un cheval.
- `Program.cs`: Ce fichier contient la classe principale `Program`, qui démontre l'utilisation de la classe `Cheval`.

## Fonctionnalités

### Gestion des Chevaux

- La classe `Cheval` permet de représenter les caractéristiques d'un cheval, telles que son nom, son âge, sa taille, son poids et sa vitesse.
- Elle fournit des méthodes pour récupérer ces informations individuellement (`GetNom()`, `GetAge()`, `GetTaille()`, `GetPoids()`, `GetVitesse()`), ainsi qu'un constructeur pour initialiser ces valeurs.

### Démonstration de l'utilisation

- La classe `Program` démontre l'utilisation de la classe `Cheval` en créant une instance de `Cheval` avec des valeurs spécifiques (dans ce cas, le cheval s'appelle "Jolly Jumper", a 3 ans, mesure 1.50m, pèse 500 kg et sa vitesse est initialisée à 0).
- Après avoir créé l'instance, elle affiche les détails du cheval à l'aide de la méthode `ToString()`.

## Possibilités d'extension

1. **Ajout de fonctionnalités de gestion supplémentaires :**
   - Vous pourriez étendre la classe `Cheval` pour inclure des fonctionnalités de gestion supplémentaires, telles que des méthodes pour modifier les valeurs des caractéristiques du cheval, calculer sa vitesse maximale en fonction de son âge et de son poids, etc.

2. **Interface utilisateur :**
   - Vous pourriez développer une interface utilisateur (console ou graphique) pour permettre aux utilisateurs d'ajouter, de modifier et de supprimer des informations sur les chevaux, ainsi que pour afficher des rapports ou des statistiques sur les chevaux du haras.

3. **Persistance des données :**
   - Vous pourriez implémenter la persistance des données pour sauvegarder les informations sur les chevaux dans une base de données ou un fichier, ce qui permettrait de conserver les données entre les exécutions du programme.

4. **Gestion des utilisateurs :**
   - Si nécessaire, vous pourriez ajouter un système de gestion des utilisateurs avec des fonctionnalités telles que l'authentification, l'autorisation et la gestion des rôles, pour contrôler l'accès aux fonctionnalités du programme.

Ces extensions pourraient rendre l'application de gestion de haras plus complète et fonctionnelle pour répondre à divers besoins et cas d'utilisation.


## Cas d'utilisation
```plantuml
@startuml

left to right direction

actor Utilisateur as User
rectangle "Gestion de Haras" {
    usecase "Ajouter un Cheval" as AddHorse
    usecase "Modifier un Cheval" as EditHorse
    usecase "Supprimer un Cheval" as DeleteHorse
    usecase "Afficher les Détails d'un Cheval" as ViewHorseDetails
    usecase "Afficher les Chevaux" as ViewHorses

    User --> AddHorse
    User --> EditHorse
    User --> DeleteHorse
    User --> ViewHorseDetails
    User --> ViewHorses
}

@enduml
```

## Description des codes
### Haras.cs
```csharp
// Fichier Cheval.cs

using System;

namespace CentreEquestre
{
    class Cheval
    {
        // Déclaration des attributs privés de la classe Cheval
        private string nom;
        private int age;
        private decimal taille;
        private int poids;
        private int vitesse;

        // Constructeur par défaut de la classe Cheval
        public Cheval()
        {
            // Le constructeur par défaut est vide
        }

        // Constructeur paramétré de la classe Cheval
        public Cheval(string nom, int age, decimal taille, int poids, int vitesse)
        {
            // Initialisation des attributs avec les valeurs passées en paramètres
            this.nom = nom;
            this.age = age;
            this.taille = taille;
            this.poids = poids;
            // La vitesse est initialisée à 0 pour un nouveau cheval
            this.vitesse = 0;
        }

        // Méthodes pour récupérer les valeurs des attributs
        public string GetNom()
        {
            return this.nom;
        }
        public int GetAge()
        {
            return this.age;
        }
        public decimal GetTaille()
        {
            return this.taille;
        }
        public int GetPoids()
        {
            return this.poids;
        }
        public int GetVitesse()
        {
            return this.vitesse;
        }

        // Redéfinition de la méthode ToString pour afficher les détails du cheval
        public new string ToString()
        {
            // Formatage des informations du cheval sous forme d'une chaîne de caractères
            return string.Format("{0}, {1}, {2}, {3}, {4}", this.nom, this.age, this.taille, this.poids, this.vitesse);
        }
    }
}
```
### Program.cs
```csharp
// Fichier Program.cs

using System;

namespace CentreEquestre
{
    class Program
    {
        static void Main(string[] args)
        {
            // Création d'une instance de Cheval avec des valeurs spécifiques
            Cheval c = new Cheval("Jolly Jumper", 3, 1.50m, 500, 0);
            
            // Affichage des détails du cheval à l'aide de la méthode ToString()
            Console.WriteLine(c.ToString());
        }
    }
}
```