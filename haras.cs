using System;

namespace CentreEquestre
{
    class Cheval
    {
        private string nom;
        private int age;
        private decimal taille;
        private int poids;
        private int vitesse;

        public Cheval()
        {
            
        }
        public string GetNom()
        {
            return this.nom;
        }
        public int GetAge()
        {
            return this.age;
        }
        public decimal GetTaille()
        {
            return this.taille;
        }
        public int GetPoids()
        {
            return this.poids;
        }
        public int GetVitesse()
        {
            return this.vitesse;
        }

        public Cheval(string nom, int age, decimal taille, int poids, int vitesse)
        {
            this.nom = nom;
            this.age = age;
            this.taille = taille;
            this.poids = poids;
            this.vitesse = 0;
        }





        public new string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}, {4}", this.nom, this.age, this.taille, this.poids, this.vitesse);
        }

    }
}
